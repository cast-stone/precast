##Outlook signature creator

A simple php script to help quickly create html signatures for employees to copy and paste into outlook. 

*Signature creator for outlook. You can [test the demo here](https://premier-stoneworks.com/sig.php) *

---

## How to use

1. download sig.php file and install on a web server that has PHP
2. Edit the signature table html to your liking
3. thats it.
---

##Screenshot

![outlook signature creator](https://bitbucket.org/cast-stone/precast/raw/b73c8a1cc0ca70c7b5f9bdb650153d5ddc841b9f/images/sig.png)

---

We got tired of everyone having different signatures so we settled on a html template that everyone should use, then we made a little form in PHP so that employees could create their own signatures and copy/paste into outlook.

We are a [cast stone manufacturer](https://premier-stoneworks/products/cast-stone/) and a [precast concrete panel manufacturer](https://premier-stoneworks.com/products/architectural-precast-concrete/).
