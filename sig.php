<?php error_reporting(E_ERROR | E_WARNING | E_PARSE); ?>
<html>
<head></head>
<style>
.myform {
  font-family: Helvetica,Arial;
}
.myform form {
  text-align: right;
  width: 400px
}
.myform form input[type=text]{
  width:210px;
  font-size:15px;
  background-color:#f2edc1;
  border:1px solid #777777;
  padding:3px;
}
</style>
<body>
  <h1>Signature Creator</h1>
  <hr>
	<div class="myform">
     <form action="sig.php" method="post">
   		<p>Full name: <input type="text" value="<?php echo isset($_POST['firstname']) ? $_POST['firstname'] : '' ?>" name="firstname" /></p>
   		
      <p>Title: <input type="text" name="title" value="<?php echo isset($_POST['title']) ? $_POST['title'] : '' ?>" name="title" /></p>
      <p>Work Ph &amp; extension: <input type="text" name="telephone" value="<?php echo isset($_POST['telephone']) ? $_POST['telephone'] : '' ?>" name="telephone" /></p>
      <p>Cell: <input type="text" name="cell" value="<?php echo isset($_POST['cell']) ? $_POST['cell'] : '' ?>" name="cell" /></p>
      <p>Email: <input type="text" name="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>" name="email" /></p>
   		<input type="submit" name="submit" value="Submit" />
		</form>
    </div>
    <div class="postedData">
      <p>Highlight the signature below the line and paste to outlook signatures</p>
      <hr>
<?php
   if(isset($_POST['submit']) )
   { 



    ?>

 <table>
  <tr>

    <td>
      <b><span style="font-size:16.0pt;font-family:'Garamond',serif;color:#3B3838"><?= $_POST['firstname'] ?> </span></b><br>
      <i><span style="font-size:13.0pt;font-family:'Garamond' serif,Times;color:#7F7F7F"><?= $_POST['title'] ?></span></i><br>
      
      <span style="font-size:12pt;font-family:'Garamond',serif">T. <?= $_POST['telephone'] ?></span>
<?php if(!empty($_POST['cell']) ){ ?>
      <br>
      <span style="font-size:12pt;font-family:'Garamond',serif">C. <?= $_POST['cell'] ?></span>
      <?php } ?>
      <br>
      <span style="font-size:12pt;font-family:'Garamond',serif;color:#3B3838"><a href="mailto:<?= $_POST['email'] ?>"><?= $_POST['email'] ?></a></span><br><br>
      

      <span style="font-size:14.0pt;font-family:'Garamond',serif;color:#B08200">PREMIER STONEWORKS</span><br>
      <span style="font-size:12pt;font-family:'Garamond',serif">1455 SW 4<sup>th</sup> Ave, Delray Beach, FL 33444</span><br>
      
      <span style="font-size:12ptfont-family:'Garamond',serif;color:#3B3838"><a href="https://premier-stoneworks.com/">https://premier-stoneworks.com</a><br>
        <i><span style="font-size:12.0pt;font-family:'Garamond' serif,Times;color:#7F7F7F">AIA/CILB Continuing Education Provider</span></i><br>
      <i><span style="font-size:12.0pt;font-family:'Garamond' serif,Times;color:#7F7F7F">Certified Cast Stone Institute Producer Member</span></i><br>
      <table  cellpadding="0" cellspacing="0" border="0" width="150px">
        <tr>
          <td border="0" width="30" style="padding-top:8px;">
            <a class="fb" href="https://www.facebook.com/premierstoneworks/" target="_blank"><img  width="22" src="http://premier-stoneworks.com/wp-content/themes/twentysixteen-child/images/footer-fb.png"></a>
          </td>
          <td border="0"  width="30" style="padding-top:8px;">
            <a class="yt" href="https://www.youtube.com/channel/UCZuMtlR4qq7IuRgRuWRX3yw" target="_blank"><img  width="22"  src="http://premier-stoneworks.com/wp-content/themes/twentysixteen-child/images/youtube_circle-29.png"></a>

          </td>
          <td border="0" width="30" style="padding-top:8px;">
            <a class="linkdin" href="https://www.linkedin.com/company/1970537?trk=vsrp_companies_cluster_name&amp;trkInfo=VSRPsearchId%3A277181451450365164298%2CVSRPtargetId%3A1970537%2CVSRPcmpt%3Acompanies_cluster" target="_blank"><img  width="22" src="http://premier-stoneworks.com/wp-content/themes/twentysixteen-child/images/footer-linkdin.png"></a>
          </td>
          <td border="0" width="30" style="padding-top:8px;">
            <a class="hozz" href="http://www.houzz.com/pro/premierstoneworksllc/premier-stoneworks-llc" target="_blank"><img  width="22" s src="http://premier-stoneworks.com/wp-content/themes/twentysixteen-child/images/footer-hozz.png"></a>
          </td>
          <td border="0" width="30" style="padding-top:8px;">
            <a class="tw" href="https://twitter.com/PremierStonewks"><img width="22"  src="http://premier-stoneworks.com/wp-content/themes/twentysixteen-child/images/footer-tw.png"></a>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

   <?php
 }
   else {
      echo "Please fill out form above";
   }
?>
    </div>
</body>
</html>